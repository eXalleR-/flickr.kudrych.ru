<div class="images form">
<?php echo $this->Form->create('Image'); ?>
	<fieldset>
		<legend><?php echo __('Edit Image'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('img_1');
		echo $this->Form->input('img_1_source');
		echo $this->Form->input('img_2');
		echo $this->Form->input('img_2_source');
		echo $this->Form->input('img_3');
		echo $this->Form->input('img_3_source');
		echo $this->Form->input('img_4');
		echo $this->Form->input('img_4_source');
		echo $this->Form->input('collage');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Image.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Image.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Images'), array('action' => 'index')); ?></li>
	</ul>
</div>
