<div class="images view">
<h2><?php echo __('Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($image['Image']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 1'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 1 Source'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_1_source']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 2'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 2 Source'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_2_source']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 3'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 3 Source'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_3_source']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 4'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img 4 Source'); ?></dt>
		<dd>
			<?php echo h($image['Image']['img_4_source']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Collage'); ?></dt>
		<dd>
			<?php echo h($image['Image']['collage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($image['Image']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Image'), array('action' => 'edit', $image['Image']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Image'), array('action' => 'delete', $image['Image']['id']), null, __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('action' => 'add')); ?> </li>
	</ul>
</div>
