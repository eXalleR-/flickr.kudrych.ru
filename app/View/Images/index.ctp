<div class="images index">
	<h2><?php echo __('Images'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('img_1'); ?></th>
			<th><?php echo $this->Paginator->sort('img_1_source'); ?></th>
			<th><?php echo $this->Paginator->sort('img_2'); ?></th>
			<th><?php echo $this->Paginator->sort('img_2_source'); ?></th>
			<th><?php echo $this->Paginator->sort('img_3'); ?></th>
			<th><?php echo $this->Paginator->sort('img_3_source'); ?></th>
			<th><?php echo $this->Paginator->sort('img_4'); ?></th>
			<th><?php echo $this->Paginator->sort('img_4_source'); ?></th>
			<th><?php echo $this->Paginator->sort('collage'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($images as $image): ?>
	<tr>
		<td><?php echo h($image['Image']['id']); ?>&nbsp;</td>
		<td>
			<a href="<?= $image['Image']['img_1'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_1'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_1_source'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_1_source'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_2'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_2'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_2_source'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_2_source'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_3'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_3'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_3_source'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_3_source'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_4'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_4'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['img_4_source'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['img_4_source'] ?>" height="50">
			</a>
		</td>
		<td>
			<a href="<?= $image['Image']['collage'] ?>" target="_blank" class="cbox">
				<img src="<?= $image['Image']['collage'] ?>" height="50">
			</a>
		</td>
		<td style="vertical-align: middle; text-align: center"><?= $this->Time->format('d.m.y - H:i', $image['Image']['created'], null, null); ?></td>
		<td class="actions" style="vertical-align: middle; text-align: center">
			<? if ( ! $image['Image']['img_1']): ?>
				<?php echo $this->Html->link(__('Make Previews'), array('action' => 'preview', $image['Image']['id'])); ?>
			<? endif ?>			
			<? if ($image['Image']['img_1'] && ! $image['Image']['collage']): ?>
				<? echo $this->Html->link(__('Make Collage'), array('action' => 'collage', $image['Image']['id'])); ?>
			<? endif ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $image['Image']['id']), null, __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?>		
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Load Images'), array('action' => 'add')); ?></li>
	</ul>
</div>
