<?php
App::uses('AppModel', 'Model');
/**
 * Image Model
 *
 */
class Image extends AppModel {

	public function createThumb($id, $filename, $name) {

		$new_height = 100;
		$new_width = 100;

		// получение новых размеров
		list($width, $height) = getimagesize($filename);

		if ($width >= $height) {
			$cut_x = ($width - $height) / 2;
			$cut_y = 0;
			$cut_w = $height;
			$cut_h = $height;
		} else {
			$cut_x = 0;
			$cut_y = ($height - $width) / 2;;
			$cut_w = $width;
			$cut_h = $width;
		}

		$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromjpeg($filename);
		imagecopyresampled($image_p, $image, 0, 0, $cut_x, $cut_y, $new_width, $new_height, $cut_w, $cut_h);

		// вывод
		$image = imagejpeg($image_p, 'uploads/' . $id .  '/' . $name, 100);

		return 'uploads/' . $id .  '/' . $name;

	}

}
