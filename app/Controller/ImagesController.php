<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http', 'SimpleXML');
/**
 * Images Controller
 *
 * @property Image $Image
 * @property PaginatorComponent $Paginator
 */
class ImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


	public function add() {

		$http = new HttpSocket();

		$response = $http->get('http://api.flickr.com/services/rest/', 
			array(
				'method' => 'flickr.photos.getRecent',
				'api_key' => 'cb7320c11be9407a29c0dad9a0446ec1',
				'per_page' => 4
				)
			);

		App::import('Utility', 'Xml');
		$xml = Xml::build($response->body);

		$photosArrayFull = Set::reverse($xml->photos);
		$photosArray = $photosArrayFull['photos']['photo'];

		$i = 1;
		foreach ($photosArray as $photo) {		
			$data['Image']['img_' . $i . '_source'] = 'http://farm' . $photo['@farm'] . '.staticflickr.com/' . $photo['@server'] . '/' . $photo['@id'] . '_' . $photo['@secret'] . '.jpg';
			$i++;
		}

		$this->Image->create();
		$this->Image->save($data);

		$this->redirect('/');

		$this->set('photos', $photosArray);

	}


	public function preview($id = null) {

		$this->autoRender = false;

		$entry = $this->Image->read(null, $id);
		$folder = 'uploads/' . $id;

		if ( ! file_exists($folder) ) {

			mkdir($folder);

			// public function Image::createThumb located in /Models/Image.php
			for ( $i = 1; $i <= 4 ; $i++) { 
				$entry['Image']['img_' . $i] = $this->Image->createThumb($id, $entry['Image']['img_' . $i . '_source'], 'image' . $i . '.jpg');
			}
			
			$this->Image->id = $entry['Image']['id'];
			$this->Image->save($entry);

			$this->Session->setFlash(__('Previews has been creared'));
			$this->collage($id);

		} else {

			$this->Session->setFlash(__('There is folder with thumbs'));
			$this->redirect('/');

		}	

	}


	public function collage($id = null) {

		$this->autoRender = false;

		$entry = $this->Image->read(null, $id);
		$folder = 'uploads/' . $entry['Image']['id'];

		$collage = imagecreatetruecolor(200, 200);

		for ( $i = 1; $i <= 4 ; $i++) { 
			$images[] = imagecreatefromjpeg($entry['Image']['img_' . $i]);
		}

		imagecopyresampled($collage, $images[0], 0,   0,   0, 0, 100, 100, 100, 100);
		imagecopyresampled($collage, $images[1], 0,   100, 0, 0, 100, 100, 100, 100);
		imagecopyresampled($collage, $images[2], 100, 0,   0, 0, 100, 100, 100, 100);
		imagecopyresampled($collage, $images[3], 100, 100, 0, 0, 100, 100, 100, 100);

		imagejpeg($collage, 'uploads/' . $id .  '/collage.jpg', 100);
		$entry['Image']['collage'] = 'uploads/' . $id .  '/collage.jpg';

		$this->Image->id = $entry['Image']['id'];
		$this->Image->save($entry);

		$this->Session->setFlash(__('Collage has been creared'));
		$this->redirect('/');

	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Image->recursive = 0;
		$this->Paginator->settings = array('order' => 'Image.id DESC');
		$this->set('images', $this->Paginator->paginate());
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		$options = array('conditions' => array('Image.' . $this->Image->primaryKey => $id));
		$this->set('image', $this->Image->find('first', $options));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Image->exists($id)) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Image.' . $this->Image->primaryKey => $id));
			$this->request->data = $this->Image->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Image->delete()) {

			// Remove dir

			$dirPath = 'uploads/' . $id;

			if (is_dir($dirPath)) {
		         if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			        $dirPath .= '/';
			    }
			    $files = glob($dirPath . '*', GLOB_MARK);
			    foreach ($files as $file) {
			        if (is_dir($file)) {
			            self::deleteDir($file);
			        } else {
			            unlink($file);
			        }
			    }
			    rmdir($dirPath);
		    }		   

			$this->Session->setFlash(__('The image has been deleted.'));
		} else {
			$this->Session->setFlash(__('The image could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
